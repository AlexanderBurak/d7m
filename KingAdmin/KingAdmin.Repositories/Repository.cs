﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingAdmin.Repositories
{
    public class Repository<T> : IDisposable where T : class
    {
        protected UnitOfWork uow;
        protected DataBaseContext context;
        protected DbSet<T> dbSet;


        public Repository(UnitOfWork uow)
        {
            this.context = uow.context;
            this.dbSet = uow.context.Set<T>();
            this.uow = uow;
        }

        public virtual IEnumerable<T> GetAll()
        {
            return dbSet;

        }

        public virtual T Get(int id)
        {
            return dbSet.Find(id);

        }

        public virtual T Add(T item)
        {
            dbSet.Add(item);
            context.SaveChanges();

            return item;
        }

        public void Remove(int id)
        {
            T item = dbSet.Find(id);
            dbSet.Remove(item);
            context.SaveChanges();
        }

        public void Dispose()
        {

            if (uow != null)
            {
                uow.Dispose();
            }

        }
    }
}
