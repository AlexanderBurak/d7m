﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KingAdmin.Entities;
using KingAdmin.RepositoriesInterfeces;

namespace KingAdmin.Repositories
{
    public class ProjectRepository : Repository<Project>, IProjectRepository
    {
        public ProjectRepository(UnitOfWork uow)
            : base(uow)
        {
        }

        public bool Update(Project item)
        {
            try
            {
                var project = dbSet.FirstOrDefault(x => x.Id == item.Id);
                project.CategoryId = item.CategoryId;
                project.Description = item.Description;
                project.EmployeeId = item.EmployeeId;
                context.SaveChanges();

            }

            catch (Exception exp)
            {
                throw new ArgumentException("item");
            }

            return true;
        }

        public override Project Add(Project item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            dbSet.Add(item);
            context.SaveChanges();

            return item;

        }
    }
}
