﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KingAdmin.Entities;
using KingAdmin.RepositoriesInterfeces;

namespace KingAdmin.Repositories
{
   public class EmployeeRepository: Repository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(UnitOfWork uow)
            : base(uow)
        {
        }

        public bool Update(Employee item)
        {
            try
            {
                var employee = dbSet.FirstOrDefault(x => x.Id == item.Id);
                employee.DepartmentId = item.DepartmentId;
                employee.Name = item.Name;
                employee.Position = item.Position;

                context.SaveChanges();

            }

            catch (Exception exp)
            {
                throw new ArgumentException("item");
            }

            return true;
        }

        public override Employee Add(Employee item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            dbSet.Add(item);
            context.SaveChanges();

            return item;

        }
    }
}
