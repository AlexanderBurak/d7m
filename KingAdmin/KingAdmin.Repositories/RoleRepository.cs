﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KingAdmin.Entities;
using KingAdmin.RepositoriesInterfeces;

namespace KingAdmin.Repositories
{
   public  class RoleRepository: Repository<Role>, IRoleRepository
    {
        public RoleRepository(UnitOfWork uow)
            : base(uow)
        {
        }

        public bool Update(Role item)
        {
            try
            {
                var role = dbSet.FirstOrDefault(x => x.Id == item.Id);
                role.Name = item.Name;

                context.SaveChanges();

            }

            catch (Exception exp)
            {
                throw new ArgumentException("item");
            }

            return true;
        }

        public override Role Add(Role item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            dbSet.Add(item);
            context.SaveChanges();

            return item;

        }
    }
}
