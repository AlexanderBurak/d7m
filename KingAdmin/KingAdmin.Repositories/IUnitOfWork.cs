﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingAdmin.Repositories
{
    public interface IUnitOfWork
    {
        void Save();
        void Dispose();
    }
}
