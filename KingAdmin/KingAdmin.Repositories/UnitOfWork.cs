﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingAdmin.Repositories
{
    public class UnitOfWork : IDisposable
    {
        public DataBaseContext context;

        public UnitOfWork(DataBaseContext context)
        {
            this.context = context;
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
