﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KingAdmin.Entities;
using KingAdmin.RepositoriesInterfeces;

namespace KingAdmin.Repositories
{
    public class DepartmentRepository: Repository<Department>, IDepartmentRepository
    {
        public DepartmentRepository(UnitOfWork uow)
            : base(uow)
        {
        }

        public bool Update(Department item)
        {
            try
            {
                var department = dbSet.FirstOrDefault(x => x.Id == item.Id);
                department.Name = item.Name;
     
                context.SaveChanges();

            }

            catch (Exception exp)
            {
                throw new ArgumentException("item");
            }

            return true;
        }

        public override Department Add(Department item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            dbSet.Add(item);
            context.SaveChanges();

            return item;

        }
    }
}
