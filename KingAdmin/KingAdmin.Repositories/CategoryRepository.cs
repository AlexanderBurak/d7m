﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KingAdmin.Entities;
using KingAdmin.RepositoriesInterfeces;

namespace KingAdmin.Repositories
{
    public class CategoryRepository: Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(UnitOfWork uow)
            : base(uow)
        {
        }

        public bool Update(Category item)
        {
            try
            {
                var category = dbSet.FirstOrDefault(x => x.Id == item.Id);
                category.Name = item.Name;
              
                context.SaveChanges();

            }

            catch (Exception exp)
            {
                throw new ArgumentException("item");
            }

            return true;
        }

        public override Category Add(Category item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            dbSet.Add(item);
            context.SaveChanges();

            return item;

        }
    }
}
