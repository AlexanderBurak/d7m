﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KingAdmin.Entities;

namespace KingAdmin.RepositoriesInterfeces
{
    public interface IProjectRepository
    {
        IEnumerable<Project> GetAll();
        Project Get(int id);
        Project Add(Project item);
        void Remove(int id);
        bool Update(Project item);

        void Dispose();
    }
}
