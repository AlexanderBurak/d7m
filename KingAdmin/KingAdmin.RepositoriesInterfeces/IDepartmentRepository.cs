﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KingAdmin.Entities;

namespace KingAdmin.RepositoriesInterfeces
{
    public interface IDepartmentRepository
    {
        IEnumerable<Department> GetAll();
        Department Get(int id);
        Department Add(Department item);
        void Remove(int id);
        bool Update(Department item);

        void Dispose();
    }
}
