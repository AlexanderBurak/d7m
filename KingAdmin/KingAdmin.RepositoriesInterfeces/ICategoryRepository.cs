﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KingAdmin.Entities;

namespace KingAdmin.RepositoriesInterfeces
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> GetAll();
        Category Get(int id);
        Category Add(Category item);
        void Remove(int id);
        bool Update(Category item);

        void Dispose();
    }
}
