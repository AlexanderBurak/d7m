﻿using System.ComponentModel.DataAnnotations;

namespace KingAdmin.Entities
{
    public class Department

    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Department name")]
        [MaxLength(50, ErrorMessage = "Exceed the maximum record length")]
        public string Name { get; set; }
    }
}
