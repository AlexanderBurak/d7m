﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace KingAdmin.Entities
{
    public class Project
    {
        public int Id { get; set; }
    
        [Required]
        [Display(Name = "Project Name")]
        [MaxLength(50, ErrorMessage = "Exceed the maximum record length")]
        public string Name { get; set; }
       
        [Required]
        [Display(Name = "Description")]
        [MaxLength(200, ErrorMessage = "Exceed the maximum record length")]
        public string Description { get; set; }

        [Display(Name = "Category")]
        public int? CategoryId { get; set; }
        public Category Category { get; set; }


        public List<int?> EmployeeId { get; set; }
        public List<Employee> Employee { get; set; }


    }
}
