﻿using System.ComponentModel.DataAnnotations;

namespace KingAdmin.Entities
{
   public  class Category
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Category name")]
        [MaxLength(50, ErrorMessage = "Exceed the maximum record length")]
        public string Name { get; set; }
    }
}
