﻿using System.ComponentModel.DataAnnotations;

namespace KingAdmin.Entities
{
    public class Employee
    {
        
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        [MaxLength(50, ErrorMessage = "Exceed the maximum record length")]
        public string Name { get; set; }
       

        [Display(Name = "Position")]
        [MaxLength(50, ErrorMessage = "Exceed the maximum record length")]
        public string Position { get; set; }

        [Display(Name = "Department")]
        public int? DepartmentId { get; set; }
        public Department Department { get; set; }

        [Required]
        [Display(Name = "Role")]
        public int RoleId { get; set; }
        public Role Role { get; set; }
    }
}
